// Copyright: 2015 Mohit Saini
// Author: Mohit Saini (mohitsaini1196@gmail.com)

#ifndef _APARSE_SRC_HELPERS_HPP_
#define _APARSE_SRC_HELPERS_HPP_

#include <string>

namespace aparse {
namespace helpers {

std::string GetAlphabetString(const std::string& s);

}  // namespace helpers
}  // namespace aparse

#endif  // _APARSE_SRC_HELPERS_HPP_
