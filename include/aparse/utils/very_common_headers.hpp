// Copyright: 2015 Mohit Saini
// Author: Mohit Saini (mohitsaini1196@gmail.com)

#ifndef APARSE_UTILS_VERY_COMMON_HEADERS_HPP_
#define APARSE_UTILS_VERY_COMMON_HEADERS_HPP_

namespace aparse {
using Alphabet = int32_t;
using EnclosedNonTerminal = int32_t;
}  // namespace aparse

#ifndef APARSE_DEBUG_FLAG
#define APARSE_DEBUG_FLAG true
#endif

#endif  // APARSE_UTILS_VERY_COMMON_HEADERS_HPP_
